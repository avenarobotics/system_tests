#!/bin/bash

set -e

PATH=$PATH:~/ros2_ws/src/system_tests/scripts
source ~/ros2_ws/src/system_tests/scripts/bash_utils.sh

source demo_validate_env.sh

LABEL=orange

action_coppelia_camera_unpause.sh
sleep 0.5

action_coppelia_camera_clear.sh
action_spawn.sh $LABEL random random $AMOUNT

sleep 15

action_coppelia_camera_pause.sh
sleep 0.5

script_validate_estimate_shape.sh $POSITION_ERROR $ORIENTATION_ERROR $SIZE_ERROR

echo -e "\e[32mALL TESTS PASSED. \e[0m"
exit 0
