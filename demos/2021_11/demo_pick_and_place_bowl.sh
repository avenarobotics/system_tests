#!/bin/bash

set -e 

PATH=$PATH:~/ros2_ws/src/system_tests/scripts
source ~/ros2_ws/src/system_tests/scripts/bash_utils.sh

FILTER_POLICY=${1:-nearest}
LABEL=${2:-bowl}
SOURCE_AREA=whole
DESTINATION_AREA=operating_area
REPEATS=10

runx $REPEATS script_pick_and_place.sh $FILTER_POLICY $LABEL $SOURCE_AREA $DESTINATION_AREA

echo -e "\e[32mALL TESTS PASSED. \e[0m"
exit 0