#!/bin/bash

set -e

PATH=$PATH:~/ros2_ws/src/system_tests/scripts
source ~/ros2_ws/src/system_tests/scripts/bash_utils.sh

time action_generate_objects.sh

echo -e "\e[32mALL TESTS PASSED. \e[0m"
exit 0
