#!/bin/bash

set -e 

PATH=$PATH:~/ros2_ws/src/system_tests/scripts
source ~/ros2_ws/src/system_tests/scripts/bash_utils.sh

label=plate

labels=( "orange" )

script_clear_scene.sh
action_home_position.sh

action_spawn.sh $label random 0,0,0 3
action_spawn.sh orange random random 5

echo -e "\e[32mALL TESTS PASSED. \e[0m"
exit 0
