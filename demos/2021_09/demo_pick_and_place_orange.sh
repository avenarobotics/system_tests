#!/bin/bash

set -e 

PATH=$PATH:~/ros2_ws/src/system_tests/scripts
source ~/ros2_ws/src/system_tests/scripts/bash_utils.sh

FILTER_POLICY=${1:-nearest}
LABEL=orange
REPEATS=5

script_prepare_scene.sh
action_home_position.sh
action_spawn.sh $LABEL random random 10

runx $REPEATS script_pick_and_place.sh $FILTER_POLICY $LABEL

echo -e "\e[32mALL TESTS PASSED. \e[0m"
exit 0
