#!/bin/bash

filename=$1

topics=" \
	/change_detect
	/rgb_images
	/depth_images
	/merged_ptcld_filtered
	/scene_debug_data
	/diff_ptcld
	/detections
	/detections_filtered
	/new_masks
	/missing_items_ids
	/compose_items
	/estimate_shape
	/merged_items
	/validate_data
	/octomap_filter
	/occupancy_grid
	/item_select
	/grasp
	/place
	/generated_path
	/generated_path_buffer
	/trajectory_point
	/gripper_control_status
	/joint_states
	/rosout
	"

timeout 5s ros2 bag record --qos-profile-overrides-path /root/ros2_ws/src/system_tests/scripts/policy_override.yaml --max-cache-size 1 -o $filename $topics
