#!/bin/bash

function run {
	sleep 0.1
	$@
	if [ $? -ne 0 ]; then
		module_name=$(echo "$@" | awk -F' ' '{print $4}') 
	    local TIMESTAMP=$(date '+%Y%m%d_%H%M%S')
	    local DATE=$(date '+%Y%m%d')
		echo " [!] Failed on module $module_name!at time $TIMESTAMP"
		#ros2 topic pub ..
		path=$HOME/dump_$TIMESTAMP
		echo $path
		record.sh $HOME/dump_$TIMESTAMP
		mysqldump -uavena -pavena -h 172.18.18.4 avena_db >> $HOME/dump_$TIMESTAMP/dump_avena_db.sql
		tar -czf $HOME/$TIMESTAMP"_"$module_name.tgz -C $HOME/dump_$TIMESTAMP $HOME/dump_$TIMESTAMP
		curl --upload-file $HOME/$TIMESTAMP"_"$module_name.tgz ftp://10.3.14.14/tests/$DATE/ --ftp-create-dirs
		rm -rf $HOME/dump_$TIMESTAMP
		exit 1
	fi
}

function runx() {
  	for ((n=0;n<$1;n++))
    	do run ${*:2}
  	done
}
