#!/bin/bash

source bash_utils.sh

run ros2 run cli gripper_open
run ros2 run cli generate_path_home
sleep 0.5
run ros2 run cli path_buffer_set path_to_home
run ros2 run cli path_buffer_get path_to_home
run ros2 run cli execute_move

exit 0
