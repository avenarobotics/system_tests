#!/bin/bash

SCENE=${1:-~/ros2_ws/src/avena_coppelia_addons/scenes/scenes/ava01_scene_coppelia_brain.ttt}

/opt/avena/coppelia_brain/coppeliaSim.sh $SCENE -q -s&

exit 0

