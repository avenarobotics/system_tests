#!/bin/bash

source bash_utils.sh

#run ros2 run cli generate_path_pick
#run ros2 run cli execute_move
#run ros2 run cli gripper_close
#run ros2 run cli generate_path_place
#run ros2 run cli execute_move
#run ros2 run cli gripper_open
run ros2 run cli path_buffer_get path_to_pick
run ros2 run cli execute_move
run ros2 run cli gripper_close
run ros2 run cli path_buffer_get pick_to_place
run ros2 run cli execute_move
run ros2 run cli gripper_open

exit 0
