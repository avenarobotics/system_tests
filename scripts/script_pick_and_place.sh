#!/bin/bash

set -e 

FILTER_POLICY=${1:-nearest}
LABEL=${2:-plate}
SOURCE_AREA=${3:-whole}
DESTINATION_AREA=${4:-operating_area}

action_generate_objects.sh
action_home_position.sh
action_generate_objects.sh
action_prepare_pick_and_place_data.sh $FILTER_POLICY $LABEL $SOURCE_AREA $DESTINATION_AREA
action_pick_and_place.sh

sleep 1

exit 0
