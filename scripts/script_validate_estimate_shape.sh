#!/bin/bash

set -e 

POSITION_ERROR=${1:-5}
ORIENTATION_ERROR=${2:-5}
SIZE_ERROR=${3:-10}

action_generate_objects.sh

action_validate_estimate_shape.sh $POSITION_ERROR $ORIENTATION_ERROR $SIZE_ERROR

action_coppelia_camera_unpause.sh

exit 0
