#!/bin/bash

source bash_utils.sh

FILTER_POLICY=${1:-nearest}
LABEL=${2:-orange}
SOURCE_AREA=${3:-whole}
DESTINATION_AREA=${4:-operating_area}

#POSITION: top_left, left_bottom, ....
#ORIENTATION: paralell, ...

run ros2 run cli get_occupancy_grid
run ros2 run cli cli_manager \
			label $LABEL \
			area_operator include \
			areas $SOURCE_AREA \
			policy $FILTER_POLICY \
			area_operator_place include \
			area_place $DESTINATION_AREA \
			search_shift 0.10 \
			place_positions_req 2 \
			constrain_value 10 

exit 0
