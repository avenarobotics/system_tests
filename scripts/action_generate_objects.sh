#!/bin/bash

source bash_utils.sh

run ros2 run cli security_controller
run ros2 run cli detect
run ros2 run cli filter_detections
run ros2 run cli select_new_masks
run ros2 run cli compose_items
run ros2 run cli estimate_shape
run ros2 run cli merge_items
run ros2 run cli octomap_filter
run ros2 run cli spawn_collision_items

exit 0
