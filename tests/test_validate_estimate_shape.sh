#!/bin/bash

cd "$(dirname "$0")"

source ~/ros2_ws/src/system_tests/scripts/bash_utils.sh

#action_coppelia_camera_start.sh
#sleep 10

#                                         label            mm    %    %
./generic_test_validate_estimate_shape.sh bowl       0.3    10    5    5
./generic_test_validate_estimate_shape.sh plate      0.4    10    5    5
./generic_test_validate_estimate_shape.sh orange     0.3    10    5    5
./generic_test_validate_estimate_shape.sh carrot     0.3    10    5    5
./generic_test_validate_estimate_shape.sh lipton     0.4    10    5    5
./generic_test_validate_estimate_shape.sh milk       0.3    10    5    5

echo -e "\e[32mALL TESTS PASSED. \e[0m"
exit 0
