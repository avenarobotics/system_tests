#!/bin/bash

set -e 

PATH=$PATH:~/ros2_ws/src/system_tests/scripts
source ~/ros2_ws/src/system_tests/scripts/bash_utils.sh

FILTER_POLICY=nearest
LABEL=orange
SOURCE_AREA=whole
DESTINATION_AREA=operating_area
REPEATS=2
AMOUNT=5

action_coppelia_brain_clear.sh
action_coppelia_camera_clear.sh

action_home_position.sh

action_spawn.sh $LABEL random random $AMOUNT

sleep 10

runx $REPEATS script_pick_and_place.sh $FILTER_POLICY $LABEL $SOURCE_AREA $DESTINATION_AREA

echo -e "\e[32mALL TESTS PASSED. \e[0m"
exit 0
