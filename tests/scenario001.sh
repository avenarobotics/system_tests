#!/bin/bash

set -e

PATH=$PATH:~/ros2_ws/src/system_tests/scripts
source ~/ros2_ws/src/system_tests/scripts/bash_utils.sh

# -------------------------------------------------------------------
echo -e "\e[32m* preparing scene \e[0m"
action_coppelia_brain_clear.sh
action_coppelia_camera_clear.sh

# -------------------------------------------------------------------
echo -e "\e[32m* Random objects \e[0m"

echo -e "\e[34m* virtualscene create label random position random orientation random amount 10 \e[0m"
ros2 run virtualscene create label random position random orientation random amount 10

echo -e "\e[34m* virtualscene list_items coppelia_camera \e[0m"
ros2 run virtualscene list_items coppelia_camera

echo -e "\e[34m* ask_coppelia ask_coppelia coppelia_camera \e[0m"
result=$(ros2 run ask_coppelia ask_coppelia coppelia_camera)
# echo $result
if [ $result != "10" ]; then echo "TEST FAILED! returns $result"; exit 1; fi

echo -e "\e[34m* virtualscene clear coppelia_camera \e[0m"
ros2 run virtualscene clear coppelia_camera

# -------------------------------------------------------------------
echo -e "\e[32m* Random Positions \e[0m"

echo -e "\e[34m* virtualscene create label banana position random orientation random amount 3 \e[0m"
ros2 run virtualscene create label banana position random orientation random amount 3

echo -e "\e[34m* virtualscene create banana position random 3 coppelia_camera \e[0m"
result=$(ros2 run ask_coppelia ask_coppelia coppelia_camera)
# echo $result
if [ $result != "3" ]; then echo "TEST FAILED! returns $result"; exit 1; fi

echo -e "\e[34m* virtualscene clear coppelia_camera \e[0m"
ros2 run virtualscene clear coppelia_camera

# -------------------------------------------------------------------
echo -e "\e[32m* Specific Positions \e[0m"

echo -e "\e[34m* virtualscene create label banana position 0,0,0 orientation 0.9,0.6,0.1 \e[0m"
ros2 run virtualscene create label banana position 0,0,0 orientation 0.9,0.6,0.1

echo -e "\e[34m* ask_coppelia ask_coppelia coppelia_camera banana \e[0m"
result=$(ros2 run ask_coppelia ask_coppelia coppelia_camera banana)
# echo $result
if [ $result != "1" ]; then echo "TEST FAILED! returns $result"; exit 1; fi

echo -e "\e[34m* virtualscene clear coppelia_camera \e[0m"
ros2 run virtualscene clear coppelia_camera

# -------------------------------------------------------------------
echo -e "\e[32m* Random Objects \e[0m"

echo -e "\e[34m* virtualscene create label random amount 10 coppelia_brain \e[0m"
ros2 run virtualscene create label random amount 10 coppelia_brain

echo -e "\e[34m* virtualscene list_items coppelia_brain \e[0m"
ros2 run virtualscene list_items coppelia_brain

echo -e "\e[34m* ask_coppelia ask_coppelia coppelia_brain \e[0m"
result=$(ros2 run ask_coppelia ask_coppelia coppelia_brain)
# echo $result
if [ $result != "10" ]; then echo "TEST FAILED! returns $result"; exit 1; fi

echo -e "\e[34m* virtualscene clear coppelia_brain \e[0m"
ros2 run virtualscene clear coppelia_brain

# -------------------------------------------------------------------
echo -e "\e[32m* Random Positions \e[0m"

echo -e "\e[34m* virtualscene create label banana position random amount 3 coppelia_brain \e[0m"
ros2 run virtualscene create label banana position random amount 3 coppelia_brain

echo -e "\e[34m* ask_coppelia ask_coppelia coppelia_brain \e[0m"
result=$(ros2 run ask_coppelia ask_coppelia coppelia_brain)
# echo $result
if [ $result != "3" ]; then echo "TEST FAILED! returns $result"; exit 1; fi

echo -e "\e[34m* virtualscene clear coppelia_brain \e[0m"
ros2 run virtualscene clear coppelia_brain

# -------------------------------------------------------------------
echo -e "\e[32m* Specific Positions \e[0m"
# echo "Specific Position"

echo -e "\e[34m* virtualscene create label banana position 0,0,0 orientation 0.9,0.6,0.1 coppelia_brain \e[0m"
ros2 run virtualscene create label banana position 0,0,0 orientation 0.9,0.6,0.1 coppelia_brain

echo -e "\e[34m* ask_coppelia ask_coppelia coppelia_brain banana \e[0m"
result=$(ros2 run ask_coppelia ask_coppelia coppelia_brain banana)
# echo $result
if [ $result != "1" ]; then echo "TEST FAILED! returns $result"; exit 1; fi

echo -e "\e[34m* virtualscene clear coppelia_brain \e[0m"
ros2 run virtualscene clear coppelia_brain

# -------------------------------------------------------------------
echo -e "\e[32mALL TESTS PASSED. \e[0m"

exit 0
