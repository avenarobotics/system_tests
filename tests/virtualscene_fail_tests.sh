#!/bin/bash

#wrong position format
ros2 run virtualscene create milk position -0.7,0.7,0,0,0 coppelia_camera

#typo in item type name
ros2 run virtualscene create cucumbe position 0.5,0.0,0.7,0,0,0 coppelia_camera

#typo in position flag
ros2 run virtualscene create cuttingboard postion 0.5,0.7,0.7,1.7,0,0 coppelia_camera

#type in Coppelia identifier

#lack of CoppeliaSim instance name
ros2 run virtualscene create cuttingboard postion 0.5,0.7,0.7,1.7,0,0
ros2 run virtualscene list_items
ros2 run virtualscene clear

