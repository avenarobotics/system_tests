#!/bin/bash

set -e

cd "$(dirname "$0")"

echo "Starting simple test."
./scenario001.sh
./scenario002.sh

exit 0
