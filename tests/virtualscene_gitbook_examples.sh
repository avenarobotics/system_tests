#!/bin/bash

COPPELIA_INSTANCE="coppelia_camera"

printf "Creating random items in $COPPELIA_INSTANCE\n"
ros2 run virtualscene create random 10 $COPPELIA_INSTANCE
printf "\n"

printf "Creating banana in $COPPELIA_INSTANCE\n"
ros2 run virtualscene create banana position random $COPPELIA_INSTANCE
printf "\n"

printf "Creating item on specific position in $COPPELIA_INSTANCE\n"
ros2 run virtualscene create random position 0.5,0.2,0.7,0,0,0 $COPPELIA_INSTANCE
printf "\n"

printf "Listing items in $COPPELIA_INSTANCE\n"
ros2 run virtualscene list_items $COPPELIA_INSTANCE
printf "\n"

printf "Counting bananas in $COPPELIA_INSTANCE\n"
ros2 run ask_coppelia ask_coppelia $COPPELIA_INSTANCE banana
printf "\n"

printf "Clearing scene in $COPPELIA_INSTANCE\n"
ros2 run virtualscene clear $COPPELIA_INSTANCE
printf "\n"

