#!/bin/bash

set -e

PATH=$PATH:~/ros2_ws/src/system_tests/scripts
source ~/ros2_ws/src/system_tests/scripts/bash_utils.sh

LABEL=${1:-orange}
STEP=${2:-0.3}

POSITION_ERROR=${3:-5}
ORIENTATION_ERROR=${4:-5}
SIZE_ERROR=${5:-10}

action_coppelia_camera_unpause.sh
sleep 0.5

grid_x=$(seq 0.2 $STEP 0.9)
grid_y=$(seq -0.8 $STEP 0.8)

action_coppelia_camera_clear.sh

for x in $grid_x
do 
    for y in $grid_y
    do
        action_spawn.sh $LABEL $x,$y,0.7 random 1
    done
done


sleep 15

action_coppelia_camera_pause.sh
sleep 0.5

script_validate_estimate_shape.sh $POSITION_ERROR $ORIENTATION_ERROR $SIZE_ERROR

exit 0
