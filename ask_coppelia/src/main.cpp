#include "custom_interfaces/srv/list_spawned_items.hpp"
#include <rclcpp/rclcpp.hpp>

using namespace std::chrono_literals;

using ListItemsClientSharedPtr = rclcpp::Client<custom_interfaces::srv::ListSpawnedItems>::SharedPtr;

std::vector<std::string> sendRequest(ListItemsClientSharedPtr client, std::shared_ptr<rclcpp::Node> node);
std::vector<std::string> filterResponse(std::vector<std::string>& response_to_filter,const std::string& filter);
char asciitolower(char in);


int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);

    try
    {
        if(argc<2)
            throw(std::runtime_error(std::string("proper usage: ros2 run ask_coppelia ask_coppelia coppelia_brain/coppelia_camera [type]\n")));
        std::shared_ptr<rclcpp::Node> node = rclcpp::Node::make_shared("ask_coppelia_client");
        std::string service_name = "/control_" + std::string(argv[1]) + "/list_spawned_items";
        auto client = node->create_client<custom_interfaces::srv::ListSpawnedItems>(service_name);
        std::vector<std::string> response = sendRequest(client, node);
        std::vector<std::string> filtered_response = response;
        if(argc == 3)
            filtered_response = filterResponse(response, argv[2]);
        std::cout << filtered_response.size();
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

char asciitolower(char in) {
    if (in <= 'Z' && in >= 'A')
        return in - ('Z' - 'z');
    return in;
}

std::vector<std::string> sendRequest(ListItemsClientSharedPtr client, std::shared_ptr<rclcpp::Node> node){
    auto request = std::make_shared<custom_interfaces::srv::ListSpawnedItems::Request>();
    while(!client->wait_for_service(1s))
    {
        if(!rclcpp::ok())
        {
            throw(std::runtime_error(std::string("Interrupted while waiting for the service. Exiting.")));
        }
        std::cerr << "service not available, waiting again...\n";
    }

    std::vector<std::string> result;

    auto response = client->async_send_request(request);
    if(rclcpp::spin_until_future_complete(node, response) == rclcpp::FutureReturnCode::SUCCESS)
    {
        std::for_each(
            response.get()->items.begin(),
            response.get()->items.end(),
            [&result](std_msgs::msg::String element){
              result.push_back(element.data);  
            }
        );
        
    }
    return result;
}

std::vector<std::string> filterResponse(std::vector<std::string>& response_to_filter, const std::string& filter){
    std::vector<std::string> output;
    output.clear();
    std::for_each(
        response_to_filter.begin(),
        response_to_filter.end(),
        [&output, filter](std::string item_info) {
            std::transform(
                item_info.begin(), item_info.end(),
                item_info.begin(),
                asciitolower);
            if (item_info.find(filter) != std::string::npos)
            {
                output.push_back(item_info);
            }
        });
    
    std::for_each(
        output.begin(),
        output.end(),
        [](std::string s){
            std::cerr << "[info] item: " << s << '\n';
        }
    );
    return output;
}